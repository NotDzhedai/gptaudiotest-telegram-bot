package main

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"strings"

	speech "cloud.google.com/go/speech/apiv1"
	"cloud.google.com/go/speech/apiv1/speechpb"
	texttospeech "cloud.google.com/go/texttospeech/apiv1"
	"cloud.google.com/go/texttospeech/apiv1/texttospeechpb"
	"github.com/cavaliergopher/grab/v3"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	gogpt "github.com/sashabaranov/go-gpt3"
)

type Config struct {
	Bot                *tgbotapi.BotAPI
	SpeechToTextClient *speech.Client
	TextToSpeechClient *texttospeech.Client
	GdpClient          *gogpt.Client
}

func main() {
	telegramToken := os.Getenv("TELEGRAM_TOKEN")
	openaiToken := os.Getenv("OPENAI_TOKEN")

	ctx := context.Background()

	// create bot
	bot, err := tgbotapi.NewBotAPI(telegramToken)
	if err != nil {
		log.Panic(err)
	}

	// створення клієнта для Google Cloud Speech-to-Text API
	speechClient, err := speech.NewClient(ctx)
	if err != nil {
		log.Panic("помилка створення клієнта: ", err)
	}
	defer speechClient.Close()

	// text to speech
	textClient, err := texttospeech.NewClient(ctx)
	if err != nil {
		log.Fatal(err)
	}
	defer textClient.Close()

	// clien for gpt
	gptClient := gogpt.NewClient(openaiToken)

	// config
	config := &Config{
		Bot:                bot,
		SpeechToTextClient: speechClient,
		TextToSpeechClient: textClient,
		GdpClient:          gptClient,
	}

	// налаштування бота
	bot.Debug = false

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message != nil {
			if update.Message.Text != "" {
				go handleTextUpdate(config, update)
			}
			if update.Message.Voice != nil {
				go handleVoiceUpdate(config, update)
			}
		}
	}

}

// обробити голосове повідомлення
func handleVoiceUpdate(cfg *Config, update tgbotapi.Update) {
	// завантажити голос
	filename, err := getVoice(cfg.Bot, update.Message.Voice.FileID)
	if err != nil {
		log.Println(err)
	}

	// переробити голос > текст
	transcript, err := processVoice(cfg.SpeechToTextClient, filename)
	if err != nil {
		log.Println(err)
	}

	// отримати відповідь від чату gpt
	result, err := getGdpResponseStream(cfg.GdpClient, transcript)
	if err != nil {
		log.Println(err)
	}

	n, err := textToSpeech(cfg.TextToSpeechClient, result, filename)
	if err != nil {
		log.Println(err)
	}
	defer os.Remove(n)

	// send message
	bytes, err := os.ReadFile(n)
	if err != nil {
		log.Println(err)
	}
	msg := tgbotapi.NewAudio(update.Message.Chat.ID, tgbotapi.FileBytes{
		Name:  "result",
		Bytes: bytes,
	})
	cfg.Bot.Send(msg)
}

// обродити текстове повідомлення
func handleTextUpdate(cfg *Config, update tgbotapi.Update) {
	result, err := getGdpResponseStream(cfg.GdpClient, update.Message.Text)
	if err != nil {
		log.Println(err)
	}

	msg := tgbotapi.NewMessage(update.Message.Chat.ID, result)
	cfg.Bot.Send(msg)
}

func getGdpResponseStream(client *gogpt.Client, question string) (string, error) {
	ctx := context.Background()

	req := gogpt.CompletionRequest{
		Model:            gogpt.GPT3TextDavinci003,
		MaxTokens:        300,
		Prompt:           question,
		TopP:             0.8,
		FrequencyPenalty: 0.2,
		PresencePenalty:  0.0,
		Stream:           true,
	}

	stream, err := client.CreateCompletionStream(ctx, req)
	if err != nil {
		return "", err
	}
	defer stream.Close()

	var result []string

	for {
		response, err := stream.Recv()
		if errors.Is(err, io.EOF) {
			break
		}

		if err != nil {
			fmt.Printf("Stream error: %v\n", err)
			return "", err
		}

		result = append(result, response.Choices[0].Text)

	}

	return strings.Join(result, ""), nil
}

// download voice
func getVoice(bot *tgbotapi.BotAPI, fileID string) (string, error) {
	file, err := bot.GetFile(tgbotapi.FileConfig{
		FileID: fileID,
	})
	if err != nil {
		return "", err
	}
	voiceURL := fmt.Sprintf("https://api.telegram.org/file/bot%s/%s", bot.Token, file.FilePath)
	resp, err := grab.Get(".", voiceURL)
	if err != nil {
		return "", err
	}
	defer resp.HTTPResponse.Body.Close()

	return resp.Filename, nil
}

// process voice
func processVoice(client *speech.Client, filename string) (string, error) {
	// delete after process
	defer os.Remove(filename)
	// зчитування звукового файлу
	audioData, err := os.ReadFile(filename)
	if err != nil {
		return "", fmt.Errorf("помилка зчитування аудіофайлу: %v", err)
	}

	// налаштування параметрів розпізнавання
	config := &speechpb.RecognitionConfig{
		Encoding:        speechpb.RecognitionConfig_OGG_OPUS,
		SampleRateHertz: 48000,
		LanguageCode:    "uk-UA",
	}
	audio := &speechpb.RecognitionAudio{
		AudioSource: &speechpb.RecognitionAudio_Content{Content: audioData},
	}

	// виклик методу розпізнавання мови
	resp, err := client.Recognize(context.Background(), &speechpb.RecognizeRequest{
		Config: config,
		Audio:  audio,
	})
	if err != nil {
		return "", fmt.Errorf("помилка розпізнавання мови: %v", err)
	}

	// виведення результатів розпізнавання
	var transcript string
	for _, result := range resp.Results {
		for _, alt := range result.Alternatives {
			transcript = alt.GetTranscript()
		}
	}

	return transcript, nil
}

// process text
func textToSpeech(client *texttospeech.Client, text string, filename string) (string, error) {
	//Perform the text-to-speech request on the text input with the selected
	// voice parameters and audio file type.
	req := texttospeechpb.SynthesizeSpeechRequest{
		// Set the text input to be synthesized.
		Input: &texttospeechpb.SynthesisInput{
			InputSource: &texttospeechpb.SynthesisInput_Text{Text: text},
		},
		// Build the voice request, select the language code ("en-US") and the SSML
		// voice gender ("neutral").
		Voice: &texttospeechpb.VoiceSelectionParams{
			LanguageCode: "uk-UA",
			SsmlGender:   texttospeechpb.SsmlVoiceGender_NEUTRAL,
		},
		// Select the type of audio file you want returned.
		AudioConfig: &texttospeechpb.AudioConfig{
			AudioEncoding: texttospeechpb.AudioEncoding_MP3,
		},
	}

	resp, err := client.SynthesizeSpeech(context.Background(), &req)
	if err != nil {
		return "", err
	}

	// The resp's AudioContent is binary.
	err = os.WriteFile(filename, resp.AudioContent, 0644)
	if err != nil {
		return "", err
	}

	return filename, nil
}
